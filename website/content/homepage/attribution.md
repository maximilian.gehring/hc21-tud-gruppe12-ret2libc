---
title: 'Attribution'
weight: 5
header_menu: false
---

**Image Sources:**
- [https://www.pexels.com/photo/black-farmed-eyeglasses-in-front-of-laptop-computer-577585/](https://www.pexels.com/photo/black-farmed-eyeglasses-in-front-of-laptop-computer-577585/)
- [https://www.pexels.com/photo/interior-of-office-building-325229/](https://www.pexels.com/photo/interior-of-office-building-325229/)
