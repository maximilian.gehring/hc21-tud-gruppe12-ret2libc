---
title: '🔍 Inner workings'
weight: 2
header_menu: false
---

When sending a request, TAHS checks for the existence of the *Mgmt-Token* header. When the header is found, the value in the *Mgmt-Token* header is compared with the configured value. When the values don't match, an error is returned (HTTP status 403). Next the body is parsed to identify the command that should be executed. Afterwards, the corresponding command is executed and the result is returned in the response.

Currently the following commands are available:
- `hostname`: The hostname of the host is returned.
- `ls {path}`: Lists all directories and files in `{path}`.
- `readfile {path}`: The contents of the file at `{path}` are returned.
- `help`: Lists the available commands.

A request could for example be:
```
POST localhost:8080 HTTP/1.1
Host: localhost:8080
Mgmt-Token: super-secret-token

hostname
```