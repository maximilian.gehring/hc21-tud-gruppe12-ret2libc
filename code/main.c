#define PORT 8080
#define REQUEST_BUFFER_SIZE 10000
#define RESPONSE_BUFFER_SIZE 10000000
#define RECEIVE_N_BYTES 1024
#define MAX_RESPONSE_HEADERS 8
#define BASE_PATH "/var/www/"
#define MGMT_TOKEN_PATH "token"

#define METHOD_GET 0
#define METHOD_POST 1

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <dirent.h>

#define NULL_REGION_SELECTOR(x) (x).start = NULL; (x).end = NULL;

static char global_mgmt_token[64+1];

// good for now... if we want multithreading, this needs to change
static char global_request_buffer[REQUEST_BUFFER_SIZE];
static char global_response_buffer[RESPONSE_BUFFER_SIZE];

typedef struct {
    char *start;
    char *end;
} region_selector_t;

u_int region_length(region_selector_t *region_selector) {
    return region_selector->end - region_selector->start;
}

typedef struct {
    char key[128];
    char value[128];
} response_header_t;

typedef struct {
    char *body_buffer;
    char *body_buffer_end;
    int status_code;
    response_header_t headers[MAX_RESPONSE_HEADERS];
    int header_count;
} response_t;

typedef struct {
    int s;
    char *buffer;
    char *buffer_end;
    int method;
    region_selector_t uri;
    region_selector_t header_host;
    region_selector_t header_mgmt_token;
    region_selector_t body;
    u_int content_length;
    response_t *response;
    bool response_ready;
} request_t;

// again: good for now...
static response_t global_response;
static request_t global_request;

response_t *new_response() {
    response_t *response = &global_response;
    if (response == NULL) {
        exit(1);
    }
    response->body_buffer = global_response_buffer;
    response->body_buffer_end = response->body_buffer;
    response->status_code = 0;
    for (int i = 0; i < MAX_RESPONSE_HEADERS; i++) {
        response->headers[i].key[0] = 0;
        response->headers[i].value[0] = 0;
    }
    response->header_count = 0;
    return response;
}

request_t *new_request() {
    request_t *request = &global_request;
    if (request == NULL) {
        exit(1);
    }
    request->s = 0;
    request->buffer = global_request_buffer;
    request->buffer_end = global_request_buffer;
    request->method = -1;
    NULL_REGION_SELECTOR(request->uri)
    NULL_REGION_SELECTOR(request->header_host)
    NULL_REGION_SELECTOR(request->header_mgmt_token)
    NULL_REGION_SELECTOR(request->body)
    request->content_length = 0;
    request->response = new_response();
    request->response_ready = false;
    return request;
}

char *find_header_newline(char *buffer, char *buffer_end) {
    bool last_cr = false;
    while (buffer < buffer_end) {
        if (last_cr && *buffer == '\n') {
            return buffer - 1;
        }
        // set last flag
        if (*buffer == '\r') {
            last_cr = true;
        } else {
            last_cr = false;
        }
        buffer++;
    }
    return NULL;
}

void response_add_header(response_t *response, char *key, char *value) {
    if (response->header_count + 1 >= MAX_RESPONSE_HEADERS) {
        exit(1);
    }
    response_header_t *new_header = &(response->headers[response->header_count]);
    strncpy(new_header->key, key, 127);
    strncpy(new_header->value, value, 127);
    new_header->key[127] = '\x00';
    new_header->value[127] = '\x00';
    response->header_count++;
}

void construct_str_response(request_t *request, int status_code, char *content) {
    request->response->status_code = status_code;
    strcpy(request->response->body_buffer, content);
    request->response->body_buffer_end = request->response->body_buffer + strlen(content);
    request->response_ready = true;
}

void parse_request(request_t *request) {
    printf("Parsing request.\n");
    int uri_offset;
    if (strncmp(request->buffer, "GET ", 4) == 0) {
        request->method = METHOD_GET;
        uri_offset = 4;
    } else if (strncmp(request->buffer, "POST ", 5) == 0) {
        request->method = METHOD_POST;
        uri_offset = 5;
    } else {
        return;
    }
    request->uri.start = request->buffer + uri_offset;
    request->uri.end = strpbrk(request->buffer + uri_offset, " ");
    if (request->uri.end == NULL) {
        return;
    }
    *(request->uri.end) = '\x00';

    if (strncmp(request->uri.end + 1, "HTTP/1.1\r\n", 10) != 0) {
        return;
    }

    char *current_header = request->uri.end + 11;
    while (true) {
        char *next_header_newline = find_header_newline(
            current_header, request->buffer_end
        );
        if (next_header_newline == NULL) {
            printf("Received malformed headers.\n");
            return;
        }
        *next_header_newline = '\x00';

        if (next_header_newline == current_header) {
            printf("Finished parsing header\n");
            current_header = next_header_newline + 2;
            break;
        }

        if (strncasecmp(current_header, "host: ", 6) == 0) {
            request->header_host.start = current_header + 6;
            request->header_host.end = next_header_newline;
        } else if (strncasecmp(current_header, "mgmt-token: ", 12) == 0) {
            request->header_mgmt_token.start = current_header + 12;
            request->header_mgmt_token.end = next_header_newline;
        } else if (strncasecmp(current_header, "content-length: ", 16) == 0) {
            region_selector_t header_content_length = {
                current_header + 16,
                next_header_newline
            };
            // ensure that the Content-Length header value fits
            // into the local char array (including null byte)
            if (region_length(&header_content_length) > 7) {
                return;
            }
            char value[8];
            strncpy(value, current_header + 16, 7);
            value[7] = '\x00';
            request->content_length = atoi(value);
        }

        current_header = next_header_newline + 2;
    }

    if (request->header_host.start == NULL) {
        construct_str_response(request, 400, "Host header required");
        return;
    }

    printf("Request to: %s\n", request->uri.start);

    request->body.start = current_header;
    request->body.end = current_header + request->content_length;
    if (request->body.end > request->buffer_end) {
        request->body.end = request->buffer_end;
    }
}

void send_response(request_t *request) {
    if (!request->response_ready) exit(1);
    printf(
        "Sending response [%d, len=%ld].\n",
        request->response->status_code,
        request->response->body_buffer_end - request->response->body_buffer
    );

    response_t *response = request->response;

    int body_length = response->body_buffer_end - response->body_buffer;
    char body_length_s[16];
    sprintf(body_length_s, "%d", body_length);
    response_add_header(response, "Content-Length", body_length_s);
    response_add_header(response, "Server", "TrulyAdvancedHttpServer/0.0.1");
    response_add_header(response, "Connection", "close");

    char *header = malloc(128*(MAX_RESPONSE_HEADERS*2+1));
    if (header == NULL) exit(1);

    char *header_current = header;
    header_current += sprintf(header_current, "HTTP/1.1 %d To Be Implemented :)\r\n", response->status_code);
    for (int i = 0; i < response->header_count; i++) {
        header_current += sprintf(
            header_current, "%s: %s\r\n",
            response->headers[i].key, response->headers[i].value
        );
    }
    header_current += sprintf(header_current, "\r\n");
    
    send(request->s, header, header_current - header, 0);
    send(request->s, response->body_buffer, response->body_buffer_end - response->body_buffer, 0);
}

typedef struct {
    request_t *request;
    char *current;
    char location[128];
} location_buffer;

// TODO need to look at this again
// The server sometimes crashes here?!?!
void index_redirect_response(request_t *request) {
    location_buffer data;
    data.request = request;
    data.current = data.location;


    data.current += sprintf(data.location, "http://");
    memcpy(data.current, request->header_host.start, region_length(&request->header_host));
    data.current += region_length(&request->header_host);
    sprintf(data.current, "/index.html");

    response_t *response = data.request->response;
    response_add_header(response, "Location", data.location);
    response->status_code = 302;
    data.request->response_ready = true;
}

void file_response(request_t *request) {
    char *next_dot = strpbrk(request->uri.start, ".");
    if (next_dot != NULL && next_dot >= request->uri.end) {
        construct_str_response(request, 400, "Sorry. '.' currently not allowed in the path.");
        return;
    }

    for (char *current = request->uri.start; current < request->uri.end; current++) {
        if (*current == '?') {
            *current = '\x00';
            break;
        }
    }

    char path[128];
    snprintf(path, 128, "%s%s", BASE_PATH, request->uri.start+1);
    printf("Sending file response for '%s'\n", path);

    int fd = open(path, O_RDONLY);
    if (fd == -1) {
        printf("Error sending file: %s\n", strerror(errno));
        if (errno == EACCES || errno == ENOENT) {
            construct_str_response(request, 404, "Not Found");
        } else {
            construct_str_response(request, 500, "Server Error");
        }
        return;
    }
    ssize_t bytes_read = read(fd, request->response->body_buffer, RESPONSE_BUFFER_SIZE);
    if (bytes_read == -1) {
        construct_str_response(request, 500, "Server Error");
        return;
    } else {
        close(fd);
        request->response->status_code = 200;
        request->response->body_buffer_end = request->response->body_buffer + bytes_read;
        request->response_ready = true;
    }
}

void mgmt_response(request_t *request) {
    if (request->header_mgmt_token.start == NULL) {
        construct_str_response(request, 400, "Mgmt-Token header required");
        return;
    }
    int rc = strcmp(request->header_mgmt_token.start, global_mgmt_token);
    if (rc != 0) {
        char response[64];
        snprintf(response, 64, "Mgmt-Token incorrect. Error code: %d", rc);
        construct_str_response(request, 401, response);
        return;
    }
    response_t *response = request->response;
    if (strncmp(request->body.start, "hostname", 8) == 0) {
        char *hostname = request->response->body_buffer;
        hostname[32] = '\x00';
        int rc = gethostname(hostname, 32);
        if (rc != 0) {
            construct_str_response(request, 500, "Server Error");
        } else {
            size_t len = strlen(request->response->body_buffer);
            response->body_buffer_end = response->body_buffer + len;
            response->status_code = 200;
            request->response_ready = true;
        }
    } else if (strncmp(request->body.start, "readfile ", 9) == 0) {
        int fd = open(request->body.start + 9, O_RDONLY);
        if (fd == -1) {
            if (errno == EACCES || errno == ENOENT) {
                construct_str_response(request, 404, "Not Found");
            } else {
                construct_str_response(request, 500, "Server Error");
            }
        } else {
            ssize_t bytes_read = read(fd, request->response->body_buffer, RESPONSE_BUFFER_SIZE);
            if (bytes_read == -1) {
                construct_str_response(request, 500, "Server Error");
                return;
            } else {
                close(fd);
                request->response->status_code = 200;
                request->response->body_buffer_end = request->response->body_buffer + bytes_read;
                request->response_ready = true;
            }
        }
    } else if (strncmp(request->body.start, "ls ", 3) == 0) {
        DIR *d = opendir(request->body.start + 3);
        if (d == NULL) {
            construct_str_response(request, 404, "Not Found");
        } else {
            struct dirent *dir;
            char *current = request->response->body_buffer;
            while ((dir = readdir(d)) != NULL) {
                if (dir->d_type == DT_DIR) {
                current += sprintf(current, "d: %s\n", dir->d_name);
                } else {
                current += sprintf(current, "f: %s\n", dir->d_name);
                }
            }
            request->response->body_buffer_end = current;
            request->response->status_code = 200;
            request->response_ready = true;
            closedir(d);
        }
    } else {
        construct_str_response(request, 200, "<h1>Help</h1>You can use the following commands:<br><b>readfile {path}:</b> Shows the content of the file at {path}.<br><b>ls {path}:</b> Lists all directories and files in {path}.<br><b>hostname:</b> Shows the computers hostname.");
    }
}

void handle_request(int s) {
    request_t *request = new_request();
    
    request->s = s;
    char *buffer_end = request->buffer;

    ssize_t bytes_recvd = recv(s, buffer_end, REQUEST_BUFFER_SIZE - 1, 0);
    if (bytes_recvd == -1) {
        printf("Error receiving data: %s\n", strerror(errno));
        return;
    }
    request->buffer_end = request->buffer + bytes_recvd;
    request->buffer[bytes_recvd] = '\x00';

    parse_request(request);
    if (!request->response_ready) {
        if (request->method == METHOD_GET && strcmp(request->uri.start, "/") == 0) {
            index_redirect_response(request);
        } else if (request->method == METHOD_GET) {
            file_response(request);
        } else if (request->method == METHOD_POST) {
            mgmt_response(request);
        } else {
            construct_str_response(request, 400, "Invalid Request");
        }
    }
    send_response(request);
}

void initialize_mgmt_token() {
    printf("Reading Mgmt-Token from config file.\n");
    int fd = open(MGMT_TOKEN_PATH, O_RDONLY);
    if (fd == -1) {
        printf("Error opening Mgmt-Token file: %s\n", strerror(errno));
        exit(1);
    }
    ssize_t bytes_read = read(fd, global_mgmt_token, 64);
    if (bytes_read <= 0) {
        printf("Error reading Mgmt-Token file: %s\n", strerror(errno));
        exit(1);
    }
    global_mgmt_token[bytes_read] = '\x00';
    close(fd);

    printf("Global Mgmt-Token is: %s\n", global_mgmt_token);
}

void sigalrm(int signo) {}

int main(int argc, char *argv[]) {
    initialize_mgmt_token();

    printf("Server starting.\n");

    // forking implementation uses:
    // https://stackoverflow.com/a/61993685
    struct sigaction action, old_action;
    action.sa_handler = sigalrm;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
#ifdef SA_INTERRUPT
    action.sa_flags |= SA_INTERRUPT;
#endif

    int s = socket(AF_INET, SOCK_STREAM, 6);
    if (s == -1) {
        printf("Socket creation failed.\n");
        return -1;
    }

    struct sockaddr_in bind_addr = {
        AF_INET,
        htons(8080),
        {
            0x00000000
        }
    };

    
    int rc = bind(s, (struct sockaddr *) &bind_addr, sizeof(bind_addr));
    if (rc == -1) {
        printf("Binding failed.\n");
        close(s);
        return -1;
    }

    rc = listen(s, 10);
    if (rc == -1) {
        printf("Listening Failed.\n");
        close(s);
        return -1;
    }

    int request_id = 0;

    while (true) {
        // TODO implement better rate limiting
        usleep(33000);
        
        struct sockaddr_in rem_sockaddr;
        socklen_t rem_socklen = sizeof(struct sockaddr_in);
        int rem_socket = accept(s, (struct sockaddr *) &rem_sockaddr, &rem_socklen);
        printf("### Incoming connection.\n");
        if (rem_socket == -1) {
            printf("Error: %s\n", strerror(errno));
            continue;
        }
        printf("Forking (%d)...\n", request_id);
        pid_t pid = fork();
        if (pid == -1) {
            printf("Forking failed: %s\n", strerror(errno));
            exit(1);
        }
        if (pid == 0) { // child
            printf("Child %d start...\n", request_id);
            handle_request(rem_socket);
            printf("Child %d done!\n", request_id);
            exit(0);
        } else { // parent
            printf("Parent %d\n", request_id);
            sigaction(SIGALRM, &action, &old_action);
            alarm(10);
            pid_t changed_pid = waitpid(pid, NULL, 0);
            int tmp_errno = errno;
            if (changed_pid == -1) {
                if (tmp_errno == EINTR) {
                    printf("Child took too long...\nContinuing...\n");
                } else {
                    printf("Unable to wait for child: %s\n", strerror(errno));
                    exit(1);
                }
            } else {
                printf("Child has exited!\n");
            }
            alarm(0);
            sigaction(SIGALRM, &old_action, NULL);
        }

        close(rem_socket);

        request_id++;
    }
    close(s);
    
    return 0;
}