FROM ubuntu:focal-20210416

RUN apt update && apt install -y gcc make wget nano iputils-ping ncat python3 sudo


# ### START SETUP DEBUGGING (OPTIONAL)
# ARG DEBIAN_FRONTEND=noninteractive

# RUN apt install -y gdb curl python3-pip strace

# RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales
# RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
#     dpkg-reconfigure --frontend=noninteractive locales && \
#     update-locale LANG=en_US.UTF-8
# ENV LANG en_US.UTF-8

# RUN pip3 install keystone-engine unicorn capstone ropper
# RUN bash -c "$(wget http://gef.blah.cat/sh -O -)"
# RUN echo "gef config context.clear_screen 0" >> /root/.gdbinit
# RUN echo "gef config context.nb_lines_stack 50" >> /root/.gdbinit
# ### END SETUP DEBUGGING

# Download hugo
RUN cd /tmp && wget https://github.com/gohugoio/hugo/releases/download/v0.83.1/hugo_extended_0.83.1_Linux-64bit.deb && dpkg --install hugo_extended_0.83.1_Linux-64bit.deb

# Create user, copy files and compile
RUN useradd -m user
COPY code /home/user
RUN cd /home/user && make
COPY ROOT_FLAG /root/FLAG
COPY USER_FLAG /home/user/FLAG
RUN cat /dev/urandom | head -c 32 | sha256sum | cut -d' ' -f 1 > /home/user/token

# Copy and compile the website
COPY website /tmp/website
RUN mkdir /var/www  && cd /tmp/website && hugo -D && cp -R /tmp/website/public/* /var/www

# Fix. permissions & cleanup
RUN chown -R user:user /home/user
RUN find /home/user -type f -exec chmod 444 -- {} +
RUN chmod 400 /root/FLAG
RUN chmod 444 /home/user/FLAG
RUN chmod +x /home/user/server
RUN rm -rf /tmp/hugo* /tmp/website

# For privesc:
RUN echo "user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER user
CMD cd /home/user && ./server