#!/bin/bash

set -e

docker build -t registry.git.rwth-aachen.de/maximilian.gehring/hc21-tud-gruppe12-ret2libc/tahs:latest .
docker push registry.git.rwth-aachen.de/maximilian.gehring/hc21-tud-gruppe12-ret2libc/tahs:latest