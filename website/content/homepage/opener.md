---
title: "🎆 Overview"
weight: 1
---

TAHS is a novel webserver with a focus on simplicity and speed. In addition TAHS advanced remote management features allow you to access your server over the HTTP port (no SSH required!). TAHS is currently still under *active development*. Once TAHS is stable enough, we will publish binaries.

Currently TAHS has the following features:
- **High Security:** TAHS uses a state of the art token based authentication scheme. No one but you (and other authorized persons 😋) can access the confidential infomation that is provided by TAHS.
- **Hostname and IP Identification:** You can query TAHS for the hostname and the IP addresses of the host. This can, for example, be useful for debugging and developing load balancer setups (and when you have forgotten the IP of your server 😉).
- **File Reading:** Did you ever need to access a file on your server while not in the office. If so, TAHS has got you covered! TAHS allows accessing files on the remote server through your browser (currently only read access is implemented, however, in the future we will support writing as well).