---
title: '🔗 Download'
weight: 3
header_menu: true
---

<a name="Download"></a>
![Nice Stock Graphic :)](/images/servers.jpg)

Once development of TAHS has progressed further, the code of will be made public and we will ship precompiled version for all major architectures.